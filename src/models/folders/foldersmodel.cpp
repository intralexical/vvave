#include "foldersmodel.h"
#include <MauiKit/FileBrowsing/fmstatic.h>

FoldersModel::FoldersModel(QObject *parent) : MauiList(parent)
{}

const FMH::MODEL_LIST &FoldersModel::items() const
{
	return this->list;
}

void FoldersModel::setFolders(const QList<QUrl> &folders)
{
	if(m_folders == folders)
		return;

	m_folders = folders;

	emit foldersChanged();
}

QList<QUrl> FoldersModel::folders() const
{
	return m_folders;
}


void FoldersModel::componentComplete()
{
    connect(this, &FoldersModel::foldersChanged, this, &FoldersModel::setList);
    this->setList();
}

void FoldersModel::setList()
{
    if(m_folders.isEmpty())
    {
        return;
    }

    emit this->preListChanged();
    this->list.clear();

    for(const auto &folder : std::as_const(m_folders))
    {
        this->list << FMStatic::getFileInfoModel(folder);
    }
    emit this->postListChanged();
    emit this->countChanged();
}
